import React from 'react';
import './App.scss';
import Home from 'containers/Home/Home';
import Login from 'containers/Login/Login';

import { Switch, Route, Redirect } from 'react-router-dom';
import Detail from 'containers/Detail/Detail';

function App() {
  const isAuthenticated = () => {
    const requestHeaders = localStorage.getItem('requestHeaders');
    return !!requestHeaders;
  };

  return (
    <Switch>
      <Route path='/login' component={Login} />
      <Route path='/:id' component={Detail} />
      <Route
        path='/'
        render={() => {
          return isAuthenticated() ? <Home /> : <Redirect to={{ pathname: '/login' }} />;
        }}
      />
    </Switch>
  );
}

export default App;
