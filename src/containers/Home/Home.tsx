import React, { useState, useEffect } from 'react';
import Nav from 'components/Nav/Nav';

import styles from './Home.module.scss';
import PreviewCard from 'components/PreviewCard/PreviewCard';
import State from 'shared/models/state.model';
import { connect } from 'react-redux';
import logo from 'assets/logo-nav.png';

import Navbar from 'react-bootstrap/Navbar';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { searchEnterprises } from 'state/actions/actions';
import Searchbar from 'components/Nav/Searchbbar/Searchbar';

interface HomeProps {
  searchCompany?: (name: string) => void;
}

const Home = (props: State & HomeProps) => {
  /** Gerencia quando mostrar ou não a barra de pesquisa */
  const [searchBarVisibility, setSearchBarVisibility] = useState(false);
  /** Gerencia quando a requisião é enviada, para controlar se o erro deve ser exibido ou não */
  const [requestDispatchedState, setRequestDispatchedState] = useState(false);
  /** Gerencia se os dados estão sendo carregados ou não */
  const [loadingState, setLoadingState] = useState(false);
  /** Estado do texto do input de pesquisa */
  const [searchTextState, setSearchTextState] = useState('');

  /** Realiza ação quando a lista de empresas é alterada */
  useEffect(() => {
    setLoadingState(false);
  }, [props.enterprises]);

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setSearchTextState(value);
  };

  const handleSearchbarToggle = () => {
    setSearchBarVisibility(previousValue => !previousValue);
    if (requestDispatchedState && searchBarVisibility) {
      setRequestDispatchedState(false);
    }
  };

  const handleSubmitSearch = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    props.searchCompany?.(searchTextState);
    setRequestDispatchedState(true);
    setLoadingState(true);
  };

  const brand = (
    <>
      <Navbar.Brand className={['mx-auto', styles['brand']].join(' ')}>
        <img src={logo} alt='ioasys white nav logo' />
      </Navbar.Brand>
      <button onClick={handleSearchbarToggle} className={styles['search-button']}>
        <FontAwesomeIcon size='lg' color='white' icon={faSearch} />
      </button>
    </>
  );

  const searchBar = (
    <Searchbar
      onSubmitSearch={handleSubmitSearch}
      searchValue={searchTextState}
      onValueChange={handleSearch}
      onToggleSearchbar={handleSearchbarToggle}
    />
  );

  let text = 'Clique na busca para iniciar.';

  /** Faz validações necessárias a exibir a mensagem de erro
   * já que a API não retorna a listagem vazia como erro.
   */
  if (
    searchBarVisibility &&
    searchTextState.length &&
    requestDispatchedState &&
    props.enterprises.length === 0 &&
    !loadingState
  ) {
    text = 'Nenhuma empresa foi encontrada para a busca realizada.';
  }

  let mainContent: JSX.Element | JSX.Element[] = <h4 style={{ margin: 'auto' }}>{text}</h4>;

  if (props.enterprises.length > 0) {
    mainContent = props.enterprises
      .map((enterprise, index) => {
        return <PreviewCard key={index} enterprise={enterprise} />;
      })
      .flat();
  }

  return (
    <div className={styles['Home']}>
      <Nav>{searchBarVisibility ? searchBar : brand}</Nav>
      <div className={[styles['main-content'], 'p-2 p-md-4'].join(' ')}>{mainContent}</div>
    </div>
  );
};

const mapStateToProps = (state: State) => {
  return state;
};

const mapDispatchToProps = (dispatch: any) => {
  return { searchCompany: (name: string) => dispatch(searchEnterprises(name)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
