import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { RouteComponentProps, Link } from 'react-router-dom';

import styles from './Detail.module.scss';

import Nav from 'components/Nav/Nav';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Company from 'shared/models/company.model';
import DetailCard from 'components/DetailCard/DetailCard';

const Detail = (props: RouteComponentProps) => {
  /** Armazena as informações recebidas da requisição
   * dos dados da empresa.
   */
  const [enterpriseState, setEnterpriseState] = useState<Company>();

  const getEnterpriseInfo = async (id: string) => {
    const requestHeaders: { ['access-token']: string; uid: string; client: string } = JSON.parse(
      localStorage.getItem('requestHeaders')!
    );

    const response = await axios.get(`https://empresas.ioasys.com.br/api/v1/enterprises/${id}`, {
      headers: {
        'access-token': requestHeaders['access-token'],
        uid: requestHeaders.uid,
        client: requestHeaders.client,
      },
    });

    setEnterpriseState(response.data.enterprise);
  };

  /** Extrai o parâmetro quando o componente inicia
   * para poder realizar a requisição ao backend.
   */
  useEffect(() => {
    const { id } = props.match.params as any;

    getEnterpriseInfo(id);
  }, [props.match.params]);

  const navBar = (
    <div className={styles['nav-title']}>
      <Link to='/'>
        <button className={styles['back-button']}>
          <FontAwesomeIcon color='white' size='lg' icon={faArrowLeft} />
        </button>
      </Link>
      <h4>{enterpriseState?.enterprise_name}</h4>
    </div>
  );

  return (
    <div className={styles['Detail']}>
      <Nav>{navBar}</Nav>
      <div className={[styles['main-content'], 'p-2 p-md-4'].join(' ')}>
        {enterpriseState ? <DetailCard enterprise={enterpriseState} /> : null}
      </div>
    </div>
  );
};

export default Detail;
