import React, { useEffect } from 'react';

import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import styles from './Searchbar.module.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle, faSearch } from '@fortawesome/free-solid-svg-icons';

interface SearchbarProps {
  searchValue: string;
  onValueChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onToggleSearchbar: () => void;
  onSubmitSearch: (event: React.FormEvent<HTMLFormElement>) => void;
}

const Searchbar = (props: SearchbarProps) => {
  const inputRef = React.createRef<HTMLInputElement>();

  useEffect(() => {
    inputRef?.current?.focus();
  }, [inputRef]);

  return (
    <Form className={['mx-auto', styles['search-bar']].join(' ')} onSubmit={props.onSubmitSearch}>
      <InputGroup>
        <FormControl
          ref={inputRef}
          value={props.searchValue}
          onChange={props.onValueChange}
          type='search'
          required
          placeholder='Search'
        />
        <InputGroup.Append className={styles['append']}>
          <button disabled={props.searchValue.length === 0} type='submit'>
            <FontAwesomeIcon color='white' size='lg' icon={faSearch} />
          </button>
          <button type='reset' onClick={props.onToggleSearchbar}>
            <FontAwesomeIcon color='white' size='lg' icon={faTimesCircle} />
          </button>
        </InputGroup.Append>
      </InputGroup>
    </Form>
  );
};

export default Searchbar;
