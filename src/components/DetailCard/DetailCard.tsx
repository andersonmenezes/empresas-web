import React from 'react';

import styles from './DetailCard.module.scss';

import Card from 'react-bootstrap/Card';

import Company from 'shared/models/company.model';

interface DetailCardProps {
  enterprise: Company;
}

const DetailCard = (props: DetailCardProps) => {
  return (
    <Card className={styles['DetailCard']}>
      <div className={styles['id']}>
        <h3>{`E${props.enterprise.id}`}</h3>
      </div>
      <div className={styles['card-body']}>
        <h5>{props.enterprise.enterprise_name}</h5>
        <h6>{`${props.enterprise.enterprise_type.enterprise_type_name} | ${props.enterprise.country}`}</h6>
        <small>{props.enterprise.description}</small>
      </div>
    </Card>
  );
};

export default DetailCard;
