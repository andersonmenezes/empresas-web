import { SAVE_ENTERPRISES } from './actions/actiontTypes';
import { clone } from 'ramda';
import State from 'shared/models/state.model';

const initialState: State = {
  enterprises: [],
};

interface Action {
  enterprises: [];
  type: string;
}

const empresasApp = (state = initialState, action: Action) => {
  switch (action.type) {
    case SAVE_ENTERPRISES: {
      const clonedState = clone(state);
      clonedState.enterprises = action.enterprises;
      return clonedState;
    }
    default: {
      return state;
    }
  }
};

export default empresasApp;
